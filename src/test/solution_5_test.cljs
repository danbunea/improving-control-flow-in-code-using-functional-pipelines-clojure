(ns solution-5-test
  (:require [cljs.test :refer (deftest testing is use-fixtures)]
            [pipes.solution-5 :refer [offer-by-id]]))

(use-fixtures :each (fn [f]
                      (reset! pipes.repo/repository pipes.repo/initial-repository)
                      (f )))

(deftest pipeline-flag-endpoint-should
  (testing "return an offer 200"
    (is (= "{\"code\":200,\"body\":{\"offer-id\":200,\"percentage\":15,\"active\":true,\"requests\":1}}" (offer-by-id "200"))))
  (testing "return a validation error 400 when not found"
    (is (= "{\"code\":400,\"body\":{\"errors\":[\"The id you provided is invalid\"]}}" (offer-by-id 400))))
  (testing "return a validation error 400 when not active"
    (is (= "{\"code\":400,\"body\":{\"errors\":[\"The offer expired\"]}}" (offer-by-id "400"))))
  (testing "return a not found 404"
    (is (= "{\"code\":404,\"body\":{\"errors\":[\"The id you provided cannot be found\"]}}" (offer-by-id "404")))))

