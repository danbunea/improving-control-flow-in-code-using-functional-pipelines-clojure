(ns pipes.repo)

(def initial-repository {:200
      {:offer-id   200,
       :percentage 15,
       :active     true,
       :requests   0},
      :400
      {:offer-id   400,
       :percentage 22,
       :active     false,
       :requests   0}})

(def repository (atom initial-repository))

(defn inc-requests [repository-atom offer-id]

  (swap! repository-atom update-in [(keyword offer-id) :requests] inc))


(comment

  @repository

  (inc-requests repository "200")
  (swap! repository update-in [:200 :requests] inc)
  )

