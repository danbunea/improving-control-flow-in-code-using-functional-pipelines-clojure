(ns pipes.solution-6
  (:require [pipes.repo :refer [repository inc-requests]]
            [pipes.notifier :refer [notify]]
            [pipes.jsonifier :refer [json]]))

(defn validate-id [state]
  (if (not (string? (:id state)))
    (throw (ex-info :ValidationException {:code 400
                                          :body {:errors ["The id you provided is invalid"]}}))
    state))

(defn find-offer-by-id [state]
  (assoc state :response (if-let [response ((keyword (:id state)) @repository)]
                           {:code 200
                            :body response}
                           ;;else
                           (do
                             (notify "not found")
                             (throw (ex-info :NotFoundException {:code 404 :body {:errors ["The id you provided cannot be found"]}}))))))

(defn update-offer-requests [state repo-atom]
  (do
    (inc-requests repo-atom (:id state))
    state))

(defn offer-not-expired [state repo-item]
  (if (:active ((keyword (:id state)) @repo-item))
    (assoc state :response {:code 200 :body ((keyword (:id state)) @repo-item)})
    (do
      (notify "offer expired")
      (assoc state :response {:code 400 :body {:errors ["The offer expired"]}})
      )))

(defn json-response [state]
  (json (:response state)))

(defn safe [func]
  (try
    (func)
    (catch :default e
      (json (ex-data e)))))

(defn offer-by-id [request-id]
  (safe
    #(-> {:id request-id}
         validate-id
         find-offer-by-id
         (update-offer-requests repository)
         (offer-not-expired repository)
         json-response)))


(comment
  (offer-by-id "200")
  (offer-by-id 400)
  (offer-by-id "400")
  (offer-by-id "404")
  )


