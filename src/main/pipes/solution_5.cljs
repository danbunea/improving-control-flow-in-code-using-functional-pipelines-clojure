(ns pipes.solution-5
  (:require [pipes.repo :refer [repository inc-requests]]
            [pipes.notifier :refer [notify]]
            [pipes.jsonifier :refer [json]]))

(defn validate-id [state]
  (if (not (string? (:id state)))
    (assoc state :response {:code 400
                            :body {:errors ["The id you provided is invalid"]}})
    state))

(defn find-offer-by-id [state]
  ;guard
  (if (:response state)
    state
    (if ((keyword (:id state)) @repository)
      state
      (assoc state :response (do
                               (notify "not found")
                               {:code 404 :body {:errors ["The id you provided cannot be found"]}})))))

(defn update-offer-requests [state repo-atom]
  (if (:response state)
    state
    (do
      (inc-requests repo-atom (:id state))
      state)))

(defn offer-not-expired [state repo-atom]
  (if (:response state)
    state
    (if (:active ((keyword (:id state)) @repo-atom))
      (assoc state :response {:code 200 :body ((keyword (:id state)) @repo-atom)})
      (do
        (notify "offer expired")
        (assoc state :response {:code 400 :body {:errors ["The offer expired"]}})
        ))))

(defn json-response [state]
  (json (:response state)))

(defn offer-by-id [request-id]
  (-> {:id request-id}
      validate-id
      find-offer-by-id
      (update-offer-requests repository)
      (offer-not-expired repository)
      json-response))


(comment
  (offer-not-expired {:id "200"} repository)
  (offer-by-id "200")
  (offer-by-id 400)
  (offer-by-id "400")
  (offer-by-id "404")
  )


