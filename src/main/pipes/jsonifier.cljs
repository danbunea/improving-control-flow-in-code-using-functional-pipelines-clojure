(ns pipes.jsonifier)

(defn json [obj]
  (js/JSON.stringify (clj->js obj)))

