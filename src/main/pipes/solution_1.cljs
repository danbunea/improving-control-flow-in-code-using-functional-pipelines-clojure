(ns pipes.solution-1
  (:require [pipes.repo :refer [repository inc-requests]]
            [pipes.notifier :refer [notify]]
            [pipes.jsonifier :refer [json]]))

(defn offer-by-id [id]
  (json
    (if (not (string? id))
      {:code 400
       :body {:errors ["The id you provided is invalid"]}}
      ;;else if
      (let [offer-id (keyword id)]
        (if-let [response ((keyword id) @repository)]
          (do
            (inc-requests repository offer-id)
            (if (:active response)
              {:code 200
               :body ((keyword id) @repository)}
              ;;else
              (do
                (notify "offer expired")
                {:code 400
                 :body {:errors ["The offer expired"]}})))
          ;;else
          (do
            (notify "not found")
            {:code 404
             :body {:errors ["The id you provided cannot be found"]}}))))))

(comment
  (offer-by-id "200")
  (offer-by-id 400)
  (offer-by-id "400")
  (offer-by-id "404")
  )

