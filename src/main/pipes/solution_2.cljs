(ns pipes.solution-2
  (:require [pipes.repo :refer [repository inc-requests]]
            [pipes.notifier :refer [notify]]
            [pipes.jsonifier :refer [json]]))

(defn valid-id [id]
  (if (not (string? id))
    (throw (ex-info :ValidationException "The id you provided is invalid"))
    id))

(defn create-response [response]
  (if (:active response)
    {:code 200
     :body response}
    ;;else
    (do
      (notify "offer expired")
      (throw (ex-info :ExpiredException "The offer expired")))
    ))

(defn find-offer-by-id [id]
  (if ((keyword id) @repository)
    (do
      (inc-requests repository (keyword id))
      (create-response ((keyword id) @repository))
      )
    ;;else
    (do
      (notify "not found")
      (throw (ex-info :NotFoundException "The id you provided cannot be found")))))

(defn offer-by-id [request-id]
  (try
    (let [id (valid-id request-id)
          response (find-offer-by-id id)]
      (json response))
    (catch :default e

      (json (cond
              (= :ValidationException (.-message e))
              {:code 400
               :body {:errors [(ex-data e)]}}

              (= :ExpiredException (.-message e))
              {:code 400
               :body {:errors [(ex-data e)]}}

              (= :NotFoundException (.-message e))
              {:code 404
               :body {:errors [(ex-data e)]}}
              ))
      )))


(comment
  (offer-by-id "200")
  (offer-by-id 400)
  (offer-by-id "400")
  (offer-by-id "404")
  )
